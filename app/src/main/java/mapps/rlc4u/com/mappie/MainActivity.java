package mapps.rlc4u.com.mappie;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.location.LocationListener;
import android.os.Debug;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Console;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor accelerometerSensor;
    private Sensor gyroscopeSensor;
    private Sensor magnetometerSensor;

    private LocationManager locationManager;

    private int accelCount;
    private int gyroCount;
    private int magnetCount;
    private int gpsCount;

    private long startTime;

    private TextView statusLabel;
    private TextView sensorLabel;

    private TextView accelerometerRecordsLabel;
    private TextView gyroscopeRecordsLabel;
    private TextView magnetometerRecordsLabel;
    private TextView gpsRecordsLabel;
    private TextView timeRecord;

    private float[] mAccelerometerData = new float[3];
    private float[] mGyroscopeData = new float[3];
    private float[] mMagnetometerData = new float[3];




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //lock in portrait
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("Defective", "bobo");

        statusLabel = findViewById(R.id.statusText);
        sensorLabel = findViewById(R.id.sensor_list);

        accelerometerRecordsLabel = findViewById(R.id.AccelerometerRecords);
        gyroscopeRecordsLabel = findViewById(R.id.GyroscopeRecords);
        magnetometerRecordsLabel = findViewById(R.id.MagnetometerRecords);
        gpsRecordsLabel = findViewById(R.id.GPSRecords);
        timeRecord = findViewById(R.id.TimeRecord);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE); // Init Instance of class
        List<Sensor> sensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL); // Get List of all Sensors

        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE); // GET GPS Class


        StringBuilder sensorText = new StringBuilder();

        for (Sensor currentSensor : sensorList) {
            sensorText.append(currentSensor.getName()).append(
                    System.getProperty("line.separator"));
        }

        sensorLabel.setText(sensorText);


        accelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); // Acceleration Forces
        gyroscopeSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE); // Acceleration Forces
        magnetometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD); // Magnetic Forces -  work with accelerometer to get device orientationt

    }

    @Override
    protected void onStart() {
        super.onStart();
        statusLabel.setText("Status: Idle");


        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unregister all sensor listeners in this callback so they don't
        // continue to use resources when the app is stopped.
        mSensorManager.unregisterListener(this);
        locationManager.removeUpdates(locationListener);
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            double latitude=location.getLatitude();
            double longitude=location.getLongitude();
            String msg="New Latitude: "+latitude + "New Longitude: "+longitude;
            Log.d("GPS", "latitude " + Double.toString(latitude));
            Log.d("GPS", "Longitude " + Double.toString(longitude));
            gpsCount++;
            gpsRecordsLabel.setText(gpsCount + "");
            //Toast.makeText(mContext,msg,Toast.LENGTH_LONG).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public void onSensorChanged(SensorEvent sensorEvent) {
        int sensorType = sensorEvent.sensor.getType();
        switch (sensorType) {
            case Sensor.TYPE_ACCELEROMETER:
                mAccelerometerData = sensorEvent.values.clone();
                float accX = mAccelerometerData[0];
                float accY = mAccelerometerData[1];
                float accZ = mAccelerometerData[2];
                Log.d("Accelerometer", "accZ " + Float.toString(mAccelerometerData[2]));
                Log.d("Accelerometer", "accZ " + Float.toString(mAccelerometerData[2]));
                Log.d("Accelerometer", "accZ " + Float.toString(mAccelerometerData[2]));
                double total = Math.sqrt(accX * accX + accY * accY + accZ * accZ);
                accelCount++;
                accelerometerRecordsLabel.setText(accelCount + "");
                break;
            case Sensor.TYPE_GYROSCOPE:
                mGyroscopeData = sensorEvent.values.clone();
                float gyrX = mGyroscopeData[0];
                float gyrY = mGyroscopeData[1];
                float gyrZ = mGyroscopeData[2];
                Log.d("Gyroscope", "gyrX " + Float.toString(gyrX));
                Log.d("Gyroscope", "gyrY " + Float.toString(gyrY));
                Log.d("Gyroscope", "gyrZ " + Float.toString(gyrZ));
                gyroCount++;
                gyroscopeRecordsLabel.setText(gyroCount + "");
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                mMagnetometerData = sensorEvent.values.clone();
                float magX = mMagnetometerData[0];
                float magY = mMagnetometerData[1];
                float magZ = mMagnetometerData[2];
                Log.d("Magnetometer", "gyrX " + Float.toString(magX));
                Log.d("Magnetometer", "gyrY " + Float.toString(magY));
                Log.d("Magnetometer", "gyrZ " + Float.toString(magZ));
                magnetCount++;
                magnetometerRecordsLabel.setText(magnetCount + "");
                break;
            default:
                return;
        }
    }

    /**
     * Must be implemented to satisfy the SensorEventListener interface;
     * unused in this app.
     */

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void startCollection(View v){
        // Listeners for the sensors are registered in this callback and
        // can be unregistered in onStop().
        //
        // Check to ensure sensors are available before registering listeners.
        // Both listeners are registered with a "normal" amount of delay
        // (SENSOR_DELAY_NORMAL).
        accelCount = 0;
        gyroCount = 0;
        magnetCount = 0;
        gpsCount = 0;

        // 1000000 = 1 read per second
        if (accelerometerSensor != null) {
            mSensorManager.registerListener(this, accelerometerSensor, 5000);
        }
        if (magnetometerSensor != null) {
            mSensorManager.registerListener(this, magnetometerSensor, 5000);
        }

        if (gyroscopeSensor != null) {
            mSensorManager.registerListener(this, gyroscopeSensor, 5000);
        }

        // Register the listener with the Location Manager to receive location updates
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        //String locationProvider = LocationManager.GPS_PROVIDER;

        //if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        //    isLocationEnabled();
        //}

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            Log.d("Services", "Coarse Location not Enabled!");
            return;
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            Log.d("Services", "Fine Location not Enabled!");
            return;
        }

        locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
        Log.d("Services", "GPS is Enabled!");

        statusLabel.setText("Status: Collecting Data");
        startTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        Log.d("Time", startTime+ "");
    }

    public void stopCollection(View v) {
        // Unregister all sensor listeners in this callback so they don't
        // continue to use resources when the app is stopped.
        mSensorManager.unregisterListener(this);
        locationManager.removeUpdates(locationListener);

        long stopTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());

        long difSeconds = stopTime - startTime;

        accelerometerRecordsLabel.setText(accelCount/difSeconds + " Records/s");
        gyroscopeRecordsLabel.setText(gyroCount/difSeconds + " Records/s");
        magnetometerRecordsLabel.setText(magnetCount/difSeconds + " Records/s");
        gpsRecordsLabel.setText(gpsCount/difSeconds + " Records/s");
        timeRecord.setText("Time Elapsed: " + difSeconds);

        Log.d("Ending", difSeconds+ "");
        Log.d("Ending", accelCount+ "");

        statusLabel.setText("Status: Idle");
    }


}
